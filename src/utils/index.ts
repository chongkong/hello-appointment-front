export function spreadDate(dt: Date) {
  return {
    year: dt.getFullYear(),
    month: dt.getMonth(),
    date: dt.getDate(),
    hour: dt.getHours(),
    minute: dt.getMinutes(),
    second: dt.getSeconds(),
    day: dt.getDay(),
    ts: dt.getTime(),
    offset: dt.getTimezoneOffset()
  };
}

function padNum(value: number, length: number) {
  let padded = '0000' + value;
  return padded.substring(padded.length - length);
}

export function localFormat(dt: Date) {
  let {year, month, date, hour, minute} = spreadDate(dt);
  return `${padNum(year, 4)}-${padNum(month, 2)}-${padNum(date, 2)}T${padNum(hour, 2)}:${padNum(minute, 2)}`;
}

export function nowWithoutMinutes(): Date {
  let now = new Date();
  now.setMinutes(0, 0, 0);
  return now;
}

export function addTime(dt: Date, delta: {hours?: number, minutes?: number, seconds?: number}) {
  let ret = new Date();
  delta = {hours: 0, minutes: 0, seconds: 0, ...delta};
  ret.setTime(dt.getTime() + ((delta.hours! * 60 + delta.minutes!) * 60 + delta.seconds!) * 1000);
  return ret;
}
