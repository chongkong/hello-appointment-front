import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Button, Dropdown, Form, Input, Modal, Message } from 'semantic-ui-react';

import { Dto } from 'models';
import { Api, StoreState } from 'store';
import { NameTag } from 'components/atoms';
import { localFormat, nowWithoutMinutes, addTime } from 'utils';

interface AppointmentHolder {
  since: string;
  until: string;
  guest: Dto.User;
}

interface FieldValidation {
  valid: boolean;
  touched: boolean;
}

interface FormHolder {
  valid: boolean;
  guestForm: FieldValidation;
  sinceForm: FieldValidation;
  untilForm: FieldValidation;
  formErrors: string[];
}

interface DispatchProps {
  create(appointment: AppointmentHolder): void;
}

interface Props extends DispatchProps {
  users: Dto.User[];
  me: Dto.User;
  trigger: React.ReactNode;
}

interface State extends AppointmentHolder, FormHolder {
  open: boolean;
}

class AppointmentEditor extends React.Component<Props, State> {
  state: State = {
    open: false,
    since: localFormat(nowWithoutMinutes()),
    until: localFormat(addTime(nowWithoutMinutes(), {hours: 1})),
    guest: {id: -1, username: ''},
    valid: false,
    guestForm: {valid: false, touched: false},
    sinceForm: {valid: true, touched: false},
    untilForm: {valid: true, touched: false},
    formErrors: []
  };

  validate<K extends keyof State>(state: State): FormHolder {
    let partial: Partial<FormHolder> = {
      valid: true,
      formErrors: []
    };
    if (state.guest.id < 0) {
      partial.guestForm = {touched: true, valid: false};
      partial.valid = false;
    }
    let since = new Date(state.since).getTime();
    let until = new Date(state.until).getTime();
    if (isNaN(since)) {
      partial.sinceForm = {touched: true, valid: false};
      partial.valid = false;
    }
    if (isNaN(until)) {
      partial.untilForm = {touched: true, valid: false};
      partial.valid = false;
    }
    if (!isNaN(since) && !isNaN(until) && since >= until) {
      partial.formErrors!.push('Datetime since should be earlier than datetime until');
      partial.valid = false;
    }
    return partial as FormHolder;
  }

  updateGuest(guestId: number) {
    let user = this.props.users.find(u => u.id === guestId) || {id: -1, username: ''};
    this.setState({
      guest: user,
      guestForm: {touched: true, valid: true}
    });
    this.setState(this.validate);
  }

  updateSince(since: string) {
    this.setState({
      since,
      sinceForm: {touched: true, valid: true}
    });
    this.setState(this.validate);
  }

  updateUntil(until: string) {
    this.setState({
      until,
      untilForm: {touched: true, valid: true}
    });
    this.setState(this.validate);
  }

  open() {
    this.setState({open: true});
  }

  close() {
    this.setState({open: false});
  }

  create() {
    this.props.create(this.state);
    this.close();
  }

  render() {
    let userOptions = this.props.users
      .filter(user => user.id !== this.props.me.id)
      .map(user => ({
        text: user.username,
        value: user.id,
        content: <NameTag username={user.username}/>
      }));
    console.debug(this.state);

    return (
      <Modal
        trigger={this.props.trigger}
        size={'tiny'}
        onOpen={() => this.open()}
        onClose={() => this.close()}
        open={this.state.open}
      >
        <Modal.Header>Create New Promise</Modal.Header>
        <Modal.Content>
          <Form>
            <Form.Field>
              <label>Invitee</label>
              <Dropdown
                search={true}
                selection={true}
                options={userOptions}
                value={this.state.guest.id}
                placeholder={'Select invitee'}
                onChange={(e, {value}) => this.updateGuest(value as number)}
              />
            </Form.Field>
            <Form.Field>
              <label>DateTime since</label>
              <Input
                type={'datetime-local'}
                value={this.state.since}
                onChange={(e, {value}) => this.updateSince(value)}
              />
            </Form.Field>
            <Form.Field>
              <label>DateTime since</label>
              <Input
                type={'datetime-local'}
                value={this.state.until}
                onChange={(e, {value}) => this.updateUntil(value)}
              />
            </Form.Field>

            <Button.Group>
              <Button
                type="submit"
                positive={true}
                disabled={!this.state.valid}
                onClick={() => this.create()}
                content={'Create'}
              />
              <Button
                onClick={() => this.close()}
                content={'Cancel'}
              />
            </Button.Group>
          </Form>
        </Modal.Content>
        {this.state.formErrors.map(message => {
          return (
            <Message
              key={message}
              error={true}
              attached={'bottom'}
              style={{margin: 0}}
            >
              {message}
            </Message>
          );
        })}
      </Modal>
    );
  }
}

function mapDispatchToProps(dispatch: Dispatch<StoreState>): DispatchProps {
  return {
    create(appointment: AppointmentHolder) {
      dispatch(Api.createAppointment({
        since: new Date(appointment.since),
        until: new Date(appointment.until),
        guest: appointment.guest.id
      }));
    }
  };
}

export default connect(null, mapDispatchToProps)(AppointmentEditor);
