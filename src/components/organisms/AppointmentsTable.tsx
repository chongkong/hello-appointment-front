import * as React from 'react';
import { Table } from 'semantic-ui-react';
import { Data } from 'models';
import { NameTag } from 'components/atoms';

interface Prop {
  appointments: Data.Appointment[];
}

export default class AppointmentsTable extends React.Component<Prop> {
  render() {
    const appointments = this.props.appointments;
    return (
      <Table textAlign="left">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Inviter</Table.HeaderCell>
            <Table.HeaderCell>Invitee</Table.HeaderCell>
            <Table.HeaderCell>Begins at</Table.HeaderCell>
            <Table.HeaderCell>Ends at</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {appointments.map(ap => (
            <Table.Row key={ap.id}>
              <Table.Cell>{ap.id}</Table.Cell>
              <Table.Cell><NameTag username={ap.host.username}/></Table.Cell>
              <Table.Cell><NameTag username={ap.guest.username}/></Table.Cell>
              <Table.Cell>{ap.sinceDate.toLocaleString()}</Table.Cell>
              <Table.Cell>{ap.untilDate.toLocaleString()}</Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    );
  }
}
