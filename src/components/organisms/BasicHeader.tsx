import * as React from 'react';
import { connect, DispatchProp } from 'react-redux';
import { Menu, Dropdown, Input, Button, Container, Form } from 'semantic-ui-react';

import { Api, StoreState } from 'store';
import { NameTag } from 'components/atoms';

interface StateProps {
  username: string;
}

interface Props extends StateProps, DispatchProp<StoreState> {}

interface State {
  username: string;
  password: string;
}

class BasicHeader extends React.Component<Props, State> {
  state: State = {
    username: '',
    password: ''
  };

  setUsername(username: string) {
    this.setState({
      ...this.state,
      username
    });
  }

  setPassword(password: string) {
    this.setState({
      ...this.state,
      password
    });
  }

  login() {
    this.props.dispatch!(Api.login(this.state.username, this.state.password));
  }

  render() {
    type Event = React.ChangeEvent<HTMLInputElement>;
    let setUsername = (event: Event) => this.setUsername(event.target.value);
    let setPassword = (event: Event) => this.setPassword(event.target.value);
    let login = () => this.login();
    let username = this.props.username;

    const userInfo = (
      <Dropdown item={true} trigger={<NameTag username={username}/>}>
        <Dropdown.Menu>
          <Dropdown.Item onClick={() => this.props.dispatch!(Api.logout())}>
            Sign out
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );

    const loginMenu = (
      <Menu.Item>
        <Form style={{margin: 0}}>
          <Input
            icon={'user outline'}
            iconPosition={'left'}
            transparent={true}
            placeholder={'Username'}
            onChange={setUsername}
          />
          <Input
            icon={'key'}
            iconPosition={'left'}
            transparent={true}
            placeholder={'Password'}
            type="password"
            onChange={setPassword}
          />
          <Button
            type="submit"
            primary={true}
            style={{whiteSpace: 'nowrap'}}
            onClick={login}
          >
            Sign in
          </Button>
        </Form>
      </Menu.Item>
    );

    return (
      <Menu pointing={true}>
        <Container>
          <Menu.Item name="promises" active={true}>
            Promises
          </Menu.Item>
          <Menu.Menu position="right">
            {username ? userInfo : loginMenu}
          </Menu.Menu>
        </Container>
      </Menu>
    );
  }
}

function mapStateToProps(state: StoreState): StateProps {
  let currentUser = state.users[state.currentUser.id];
  return {
    username: currentUser ? currentUser.username : ''
  };
}

export default connect(mapStateToProps)(BasicHeader);
