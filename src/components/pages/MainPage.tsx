import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Button, Container, Divider, Message } from 'semantic-ui-react';

import { Data, Dto } from 'models';
import { AppointmentsTable, AppointmentEditor, BasicHeader } from 'components/organisms';
import { Err, ErrorState, Select, StoreState } from 'store';

interface StateProps {
  appointments: Data.Appointment[];
  availableUsers: Dto.User[];
  me: Dto.User;
  isSignedIn: boolean;
  error: ErrorState;
}

interface DispatchProps {
  closeError(): void;
}

interface Props extends StateProps, DispatchProps {}

class MainPage extends React.Component<Props> {
  render() {
    return (
      <div>
        <BasicHeader/>
        {this.props.error.hasError &&
          <Container>
            <Message style={{cursor: 'pointer'}} error={true} onClick={this.props.closeError}>
              <Message.Header>{this.props.error.status || 'Error'}</Message.Header>
              {this.props.error.message}
            </Message>
          </Container>
        }
        {this.props.isSignedIn &&
          <Container>
            <AppointmentEditor
              users={this.props.availableUsers}
              me={this.props.me}
              trigger={<Button primary={true} icon={'plus'} content={'New'}/>}
            />
            <Divider/>
            <AppointmentsTable appointments={this.props.appointments}/>
          </Container>
        }
      </div>
    );
  }
}

function mapStateToProps(state: StoreState): StateProps {
  return {
    appointments: state.allAppointments.items
      .sort()
      .map(Select(state).appointment),
    availableUsers: state.allUsers.items
      .filter(id => id !== state.currentUser.id)
      .map(id => state.users[id]),
    me: state.users[state.currentUser.id],
    isSignedIn: state.currentUser.id !== -1,
    error: state.error
  };
}

function mapDispatchToProps(dispatch: Dispatch<StoreState>): DispatchProps {
  return {
    closeError() {
      dispatch(Err.closeError());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
