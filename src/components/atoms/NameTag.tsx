import * as React from 'react';
import { Image } from 'semantic-ui-react';

interface Prop {
  username?: string;
}

export default class NameTag extends React.Component<Prop> {
  render() {
    let username = this.props.username;
    return (
      <div>
        <Image avatar={true} src={`https://api.adorable.io/avatars/128/${username}.png`}/>
        <span style={{opacity: username ? 1 : 0.5}}>{' ' + (username || 'Not logged in')}</span>
      </div>
    );
  }
}
