import * as React from 'react';
import { Provider } from 'react-redux';
import { MainPage } from 'components/pages';

import './App.css';
import { store } from 'store';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <MainPage/>
        </div>
      </Provider>
    );
  }
}

export default App;
