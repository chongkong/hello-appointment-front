import { ApiClient } from './client';
import { Dto, Args } from '../models/index';

export namespace AppointmentService {
  export interface LoginResult {
    me: Dto.User;
    users: Dto.User[];
    appointments: Dto.Appointment[];
  }
}

export class AppointmentService {
  constructor(
    public client: ApiClient
  ) {}

  login(username: string, password: string): Promise<AppointmentService.LoginResult> {
    this.client.setAuth(username, password);
    return Promise.all([
      this.listUsers(),
      this.listAppointments()
    ]).then(([users, appointments]) => ({
      me: users.find(user => user.username === username)!,
      users,
      appointments
    }));
  }

  logout() {
    this.client.setAuth();
  }

  listAppointments(): Promise<Dto.Appointment[]> {
    return this.client.get('/promises/');
  }

  createAppointment(args: Args.CreateAppointment): Promise<Dto.Appointment> {
    return this.client.post('/promises/', args);
  }

  editAppointment(id: number, args: Args.EditAppointment): Promise<Dto.Appointment> {
    return this.client.patch(`/promises/${id}/`, args);
  }

  deleteAppointment(id: number): Promise<void> {
    return this.client.delete(`/promises/${id}/`);
  }

  listUsers(): Promise<Dto.User[]> {
    return this.client.get('/users/');
  }
}
