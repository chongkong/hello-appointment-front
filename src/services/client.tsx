export class ApiClient {
  private username?: string;
  private password?: string;

  constructor(
    public endpoint: string = ''
  ) {}

  private get basicAuthToken(): string {
    return 'Basic ' + new Buffer(`${this.username}:${this.password}`).toString('base64');
  }

  private get headers(): {[key: string]: string} {
    let headers: {[key: string]: string} = {
      'Content-Type': 'application/json; charset=utf-8'
    };
    if (this.username && this.password) {
      headers.Authorization = this.basicAuthToken;
    }
    return headers;
  }

  setAuth(username?: string, password?: string) {
    this.username = username;
    this.password = password;
  }

  request<T>(url: string, method: string, data?: any): Promise<T> {
    return fetch(this.endpoint + url, {
      method,
      headers: this.headers,
      body: data && JSON.stringify(data)
    }).then(resp => {
      return resp.text()
        .then(text => {
          try {
            return JSON.parse(text);
          } catch (error) {
            return text;
          }
        })
        .then(val => {
          if (!resp.ok) {
            throw {
              status: `${resp.status} ${resp.statusText}`,
              message: val.detail
            };
          }
          return val;
        });
    });
  }

  get<T = any>(url: string): Promise<T> {
    return this.request(url, 'GET');
  }

  post<T = any>(url: string, data: any): Promise<T> {
    return this.request(url, 'POST', data);
  }

  put<T = any>(url: string, data: any): Promise<T> {
    return this.request(url, 'PUT', data);
  }

  patch<T = any>(url: string, data: any): Promise<T> {
    return this.request(url, 'PATCH', data);
  }

  delete<T = any>(url: string): Promise<void> {
    return this.request(url, 'DELETE');
  }
}
