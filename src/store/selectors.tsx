import { Data } from 'models';
import { StoreState } from './reducers';

export const Select = (state: StoreState) => ({
  appointment(id: number): Data.Appointment {
    let ap = state.appointments[id];
    return {
      id,
      sinceDate: new Date(ap.sinceWhen),
      untilDate: new Date(ap.tilWhen),
      host: state.users[ap.user1],
      guest: state.users[ap.user2]
    };
  }
});
