import { combineReducers } from 'redux';
import { Dto } from 'models';
import { Action, ActionKey } from './actions';

interface Collection {
  isFetching: boolean;
  items: number[];
}

interface Entity<T> {
  [id: number]: T;
}

interface UserState {
  id: number;
  isSigningIn: boolean;
}

export interface ErrorState {
  hasError: boolean;
  status: string;
  message: string;
}

export interface StoreState {
  users: Entity<Dto.User>;
  appointments: Entity<Dto.Appointment>;
  allUsers: Collection;
  allAppointments: Collection;
  currentUser: UserState;
  error: ErrorState;
}

function users(state: Entity<Dto.User> = {}, action: Action): Entity<Dto.User> {
  switch (action.type) {
    case ActionKey.LOGIN_SUCCEED: {
      let typedAction = action as Action.LoginSucceed;
      return typedAction.users.reduce((entity, val) => {
        entity[val.id] = val;
        return entity;
      }, {});
    }
    case ActionKey.LIST_USERS_SUCCEED: {
      let typedAction = action as Action.ListUsersSucceed;
      return typedAction.users.reduce((entity, val) => {
        entity[val.id] = val;
        return entity;
      }, {});
    }
    case ActionKey.LOGOUT:
      return {};
    default:
      return state;
  }
}

function appointments(state: Entity<Dto.Appointment> = {}, action: Action): Entity<Dto.Appointment> {
  switch (action.type) {
    case ActionKey.LOGIN_SUCCEED: {
      let typedAction = action as Action.LoginSucceed;
      return typedAction.appointments.reduce((entity, ap) => {
        entity[ap.id] = ap;
        return entity;
      }, {});
    }
    case ActionKey.LIST_APPOINTMENT_SUCCEED: {
      let typedAction = action as Action.ListAppointmentSucceed;
      return typedAction.appointments.reduce((entity, ap) => {
        entity[ap.id] = ap;
        return entity;
      }, {});
    }
    case ActionKey.CREATE_APPOINTMENT_SUCCEED: {
      let typedAction = action as Action.CreateAppointmentSucceed;
      let added = typedAction.appointment;
      return {...state, [added.id]: added};
    }
    case ActionKey.EDIT_APPOINTMENT_SUCCEED: {
      let typedAction = action as Action.EditAppointmentSucceed;
      let updated = typedAction.appointment;
      return {...state, [updated.id]: updated};
    }
    case ActionKey.DELETE_APPOINTMENT_SUCCEED: {
      let typedAction = action as Action.DeleteAppointmentSucceed;
      let stateCopy = {...state};
      delete stateCopy[typedAction.id];
      return stateCopy;
    }
    case ActionKey.LOGOUT:
      return {};
    default:
      return state;
  }
}

function allUsers(
  state: Collection = {
    isFetching: false,
    items: []
  },
  action: Action
): Collection {
  switch (action.type) {
    case ActionKey.LOGIN_SUCCEED: {
      let typedAction = action as Action.LoginSucceed;
      return {
        isFetching: true,
        items: typedAction.users.map(user => user.id)
      };
    }
    case ActionKey.LIST_USERS:
      return {
        isFetching: true,
        items: [...state.items]
      };
    case ActionKey.LIST_USERS_SUCCEED: {
      let typedAction = action as Action.ListUsersSucceed;
      return {
        isFetching: false,
        items: typedAction.users.map(user => user.id)
      };
    }
    case ActionKey.LOGOUT:
      return {
        isFetching: false,
        items: []
      };
    default:
      return state;
  }
}

function allAppointments(
  state: Collection = {
    isFetching: false,
    items: []
  },
  action: Action
): Collection {
  switch (action.type) {
    case ActionKey.LIST_APPOINTMENT:
    case ActionKey.CREATE_APPOINTMENT:
    case ActionKey.DELETE_APPOINTMENT: {
      return {
        isFetching: true,
        items: [...state.items]
      };
    }
    case ActionKey.LOGIN_SUCCEED: {
      let typedAction = action as Action.LoginSucceed;
      return {
        isFetching: false,
        items: typedAction.appointments.map(ap => ap.id)
      };
    }
    case ActionKey.LIST_APPOINTMENT_SUCCEED: {
      let typedAction = action as Action.ListAppointmentSucceed;
      return {
        isFetching: false,
        items: typedAction.appointments.map(ap => ap.id)
      };
    }
    case ActionKey.CREATE_APPOINTMENT_SUCCEED: {
      let typedAction = action as Action.CreateAppointmentSucceed;
      let newApId = typedAction.appointment.id;
      return {
        isFetching: false,
        items: [newApId, ...state.items]
      };
    }
    case ActionKey.DELETE_APPOINTMENT_SUCCEED: {
      let typedAction = action as Action.DeleteAppointmentSucceed;
      let deletedApId = typedAction.id;
      return {
        isFetching: false,
        items: state.items.filter(id => id !== deletedApId)
      };
    }
    case ActionKey.LOGOUT: {
      return {
        isFetching: false,
        items: []
      };
    }
    default:
      return state;
  }
}

function currentUser(
  state: UserState = {
    id: -1,
    isSigningIn: false
  },
  action: Action
): UserState {
  switch (action.type) {
    case ActionKey.LOGIN:
      return {
        id: -1,
        isSigningIn: true
      };
    case ActionKey.LOGIN_SUCCEED: {
      let typedAction = action as Action.LoginSucceed;
      return {
        id: typedAction.me.id,
        isSigningIn: false
      };
    }
    case ActionKey.LOGIN_FAILED:
    case ActionKey.LOGOUT:
      return {
        id: -1,
        isSigningIn: false
      };
    default:
      return state;
  }
}

function error(
  state: ErrorState = {
    hasError: false,
    status: '',
    message: ''
  },
  action: Action
): ErrorState {
  switch (action.type) {
    case ActionKey.DISPLAY_ERROR: {
      let typedAction = action as Action.DisplayError;
      return {
        hasError: true,
        status: typedAction.status,
        message: typedAction.message
      };
    }
    case ActionKey.CLOSE_ERROR:
      return {
        hasError: false,
        status: '',
        message: ''
      };
    default:
      return state;
  }
}

export default combineReducers({
  users,
  appointments,
  allUsers,
  allAppointments,
  currentUser,
  error
});
