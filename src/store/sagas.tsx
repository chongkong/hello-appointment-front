import { all, put, take } from 'redux-saga/effects';
import { Action, ActionKey, Api, Err } from './actions';
import { Dto } from 'models';
import { AppointmentService } from 'services';

export class AppointmentSaga {
  constructor(
    public service: AppointmentService
  ) {}

  *loginFlow() {
    while (true) {
      try {
        let action: Action.Login = yield take(ActionKey.LOGIN);
        let {username, password} = action;
        let {me, users, appointments} = yield this.service.login(username, password);
        yield put(Api.loginSucceed(me, users, appointments));
      } catch (err) {
        yield all([
          put(Api.loginFailed()),
          put(Err.displayError(err.status, err.message)),
          this.service.logout(),
        ]);
        continue;
      }
      yield take(ActionKey.LOGOUT);
      yield this.service.logout();
    }
  }

  *listUsersFlow() {
    while (true) {
      try {
        yield take(ActionKey.LIST_USERS);
        let users = yield this.service.listUsers();
        yield put(Api.listUsersSucceed(users));
      } catch (err) {
        yield put(Err.displayError(err.status, err.message));
      }
    }
  }

  *listAppointmentsFlow() {
    while (true) {
      try {
        yield take(ActionKey.LIST_APPOINTMENT);
        let appointments = yield this.service.listAppointments();
        yield put(Api.listAppointmentSucceed(appointments));
      } catch (err) {
        yield put(Err.displayError(err.status, err.message));
      }
    }
  }

  *createAppointmentFlow() {
    while (true) {
      try {
        let action: Action.CreateAppointment = yield take(ActionKey.CREATE_APPOINTMENT);
        let {args} = action;
        let appointment: Dto.Appointment = yield this.service.createAppointment(args);
        yield put(Api.createAppointmentSucceed(appointment));
      } catch (err) {
        yield put(Err.displayError(err.status, err.message));
      }
    }
  }

  *editAppointmentFlow() {
    while (true) {
      try {
        let action: Action.EditAppointment = yield take(ActionKey.EDIT_APPOINTMENT);
        let {id, args} = action;
        let appointment: Dto.Appointment = yield this.service.editAppointment(id, args);
        yield put(Api.editAppointmentSucceed(appointment));
      } catch (err) {
        yield put(Err.displayError(err.status, err.message));
      }
    }
  }

  *deleteAppointmentFlow() {
    while (true) {
      try {
        let action: Action.DeleteAppointment
          = yield take(ActionKey.DELETE_APPOINTMENT);
        let {id} = action;
        yield this.service.deleteAppointment(id);
        yield put(Api.deleteAppointmentSucceed(id));
      } catch (err) {
        yield put(Err.displayError(err.status, err.message));
      }
    }
  }

  rootSaga() {
    let self = this;
    return function* () {
      yield all([
        self.loginFlow(),
        self.listUsersFlow(),
        self.listAppointmentsFlow(),
        self.createAppointmentFlow(),
        self.editAppointmentFlow(),
        self.deleteAppointmentFlow()
      ]);
    };
  }
}
