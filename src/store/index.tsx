import { createStore, applyMiddleware, Store } from 'redux';
import { default as createSagaMiddleware } from 'redux-saga';
import { default as reducers, StoreState } from './reducers';
import { AppointmentSaga } from './sagas';
import { ApiClient, AppointmentService } from 'services';

const appointmentService = new AppointmentService(new ApiClient('/api'));
const saga = new AppointmentSaga(appointmentService);

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  reducers,
  applyMiddleware(sagaMiddleware)
) as Store<StoreState>;

sagaMiddleware.run(saga.rootSaga());

export * from './reducers';
export * from './actions';
export * from './sagas';
export * from './selectors';
