import { Dto, Args } from '../models/index';

export enum ActionKey {
  LOGIN = 'LOGIN',
  LOGIN_SUCCEED = 'LOGIN_SUCCEED',
  LOGIN_FAILED = 'LOGIN_FAILED',

  LOGOUT = 'LOGOUT',

  LIST_USERS = 'LIST_USERS',
  LIST_USERS_SUCCEED = 'LIST_USERS_SUCCEED',

  LIST_APPOINTMENT = 'LIST_APPOINTMENT',
  LIST_APPOINTMENT_SUCCEED = 'LIST_APPOINTMENT_SUCCEED',

  CREATE_APPOINTMENT = 'CREATE_APPOINTMENT',
  CREATE_APPOINTMENT_SUCCEED = 'CREATE_APPOINTMENT_SUCCEED',

  EDIT_APPOINTMENT = 'EDIT_APPOINTMENT',
  EDIT_APPOINTMENT_SUCCEED = 'EDIT_APPOINTMENT_SUCCEED',

  DELETE_APPOINTMENT = 'DELETE_APPOINTMENT',
  DELETE_APPOINTMENT_SUCCEED = 'DELETE_APPOINTMENT_SUCCEED',

  DISPLAY_ERROR = 'DISPLAY_ERROR',
  CLOSE_ERROR = 'CLOSE_ERROR',
}

export interface Action {
  type: ActionKey;
}

export namespace Action {
  export interface Login extends Action {
    username: string;
    password: string;
  }

  export interface LoginSucceed extends Action {
    me: Dto.User;
    users: Dto.User[];
    appointments: Dto.Appointment[];
  }

  export interface ListUsersSucceed extends Action {
    users: Dto.User[];
  }

  export interface CreateAppointment extends Action {
    args: Args.CreateAppointment;
  }

  export interface EditAppointment extends Action {
    id: number;
    args: Args.EditAppointment;
  }

  export interface DeleteAppointment extends Action {
    id: number;
  }

  export interface ListAppointmentSucceed extends Action {
    appointments: Dto.Appointment[];
  }

  export interface CreateAppointmentSucceed extends Action {
    appointment: Dto.Appointment;
  }

  export interface EditAppointmentSucceed extends Action {
    appointment: Dto.Appointment;
  }

  export interface DeleteAppointmentSucceed extends Action {
    id: number;
  }

  export interface DisplayError extends Action {
    status: string;
    message: string;
  }
}

export class Api {
  static login(username: string, password: string): Action.Login {
    return {type: ActionKey.LOGIN, username, password};
  }

  static loginSucceed(me: Dto.User, users: Dto.User[], appointments: Dto.Appointment[]): Action.LoginSucceed {
    return {type: ActionKey.LOGIN_SUCCEED, me, users, appointments};
  }

  static loginFailed(): Action {
    return {type: ActionKey.LOGIN_FAILED};
  }

  static logout(): Action {
    return {type: ActionKey.LOGOUT};
  }

  static listUsers(): Action {
    return {type: ActionKey.LIST_USERS};
  }

  static listUsersSucceed(users: Dto.User[]): Action.ListUsersSucceed {
    return {type: ActionKey.LIST_USERS_SUCCEED, users};
  }

  static listAppointments(): Action {
    return {type: ActionKey.LIST_APPOINTMENT};
  }

  static listAppointmentSucceed(appointments: Dto.Appointment[]): Action.ListAppointmentSucceed {
    return {type: ActionKey.LIST_APPOINTMENT_SUCCEED, appointments};
  }

  static createAppointment(data: {since: Date, until: Date, guest: number}): Action.CreateAppointment {
    return {
      type: ActionKey.CREATE_APPOINTMENT,
      args: {
        sinceWhen: data.since.toISOString(),
        tilWhen: data.until.toISOString(),
        user2: data.guest
      }
    };
  }

  static createAppointmentSucceed(appointment: Dto.Appointment): Action.CreateAppointmentSucceed {
    return {type: ActionKey.CREATE_APPOINTMENT_SUCCEED, appointment};
  }

  static editAppointment(data: {id: number, since: Date, until: Date}): Action.EditAppointment {
    return {
      type: ActionKey.EDIT_APPOINTMENT,
      id: data.id,
      args: {
        sinceWhen: data.since.toISOString(),
        tilWhen: data.until.toISOString(),
      }
    };
  }

  static editAppointmentSucceed(appointment: Dto.Appointment): Action.EditAppointmentSucceed {
    return {type: ActionKey.EDIT_APPOINTMENT_SUCCEED, appointment};
  }

  static deleteAppointment(id: number): Action.DeleteAppointment {
    return {type: ActionKey.DELETE_APPOINTMENT, id};
  }

  static deleteAppointmentSucceed(id: number): Action.DeleteAppointmentSucceed {
    return {type: ActionKey.DELETE_APPOINTMENT_SUCCEED, id};
  }
}

export class Err {
  static displayError(status: string, message: string): Action.DisplayError {
    return {type: ActionKey.DISPLAY_ERROR, status, message};
  }

  static closeError(): Action {
    return {type: ActionKey.CLOSE_ERROR};
  }
}
