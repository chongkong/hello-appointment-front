export namespace Dto {
  export interface User {
    id: number;
    username: string;
  }

  export interface Appointment {
    id: number;
    sinceWhen: string;
    tilWhen: string;
    created: string;
    user1: number;
    user2: number;
  }
}

export namespace Args {
  export interface CreateAppointment {
    sinceWhen: string;
    tilWhen: string;
    user2: number;
  }

  export interface EditAppointment {
    sinceWhen: string;
    tilWhen: string;
  }
}

export namespace Data {
  export interface Appointment {
    id: number;
    sinceDate: Date;
    untilDate: Date;
    host: Dto.User;
    guest: Dto.User;
  }
}
